# c7n-demo

Reduce cost of cloud environment by automatically starting up or shutting down EC2 instances on a given schedule.
This is particularly suited for development or test environment that only need to be available during office hours.

Based on custodian <https://github.com/cloud-custodian/cloud-custodian>

- [c7n-demo](#c7n-demo)
  - [Principle](#principle)
  - [Usage](#usage)
    - [Prerequisites](#prerequisites)
    - [Quick start: deploy all resources](#quick-start-deploy-all-resources)
      - [Optional: Override default schedule of instances](#optional-override-default-schedule-of-instances)
    - [Execute policies](#execute-policies)
      - [Manual execution (from local computer)](#manual-execution-from-local-computer)
      - [Automate execution (deploy policies as lambda)](#automate-execution-deploy-policies-as-lambda)
  - [Caveats](#caveats)
    - [The demo code use specific tags](#the-demo-code-use-specific-tags)
    - [Testing automatic off hours / on hours policy](#testing-automatic-off-hours--on-hours-policy)
    - [Preventing shutdown of an instance that was manually restarted](#preventing-shutdown-of-an-instance-that-was-manually-restarted)
    - [Resources](#resources)

## Principle

Custodian enforces automatic startup or shutdown of EC2 instances using a _policy_.

In this demo, the policy is applied _only_ to instances that have a specific tag.
This demo creates sample aws resources using terraform and provides way to apply policies to the resources.

2 modes of execution:

- Manual execution (test): execute policy from a local machine
- Automatic execution (production): the policy is executed at regular intervals using a lambda funciton deployed on AWS.

In addition, individual EC2 instances can override the the default startup/shutdown hours setting a tag with a specific schedule.

```mermaid
graph LR
  subgraph AWS
  EC2[EC2 instances]
  lambda[Lambda function]
  end
  subgraph Local machine
  custodian[Custodian policy]-->mode{Type periodic ?}
  end
  mode -->|Manual execution| EC2
  mode -->|Creates lambda for policy| lambda
  lambda[Lambda function] -->|Automatic execution| EC2[EC2 instances]

  style AWS fill:#8FB7E5
```

## Usage

### Prerequisites

- Install and configure terraform (<https://learn.hashicorp.com/terraform>)
- Install cloud custodian (see <https://cloudcustodian.io/docs/quickstart/index.html)>

> The demo code uses a terraform s3 backend. Backend configuration is linked to _your_ account and cannot be shared.
> You should create the backend with the provided `create-s3-backend` module and update `backend.tf` with _your_ backend settings.
> For a quick test, you can skip this step and use a local terraform local state by deleting `backend.tf`.

### Quick start: deploy all resources

Run `terraform/aws/main.tf` creates

- an IAM role for custodian lambdas ( `terraform/aws/custodian-infra` module)
- a set of demo EC2 instances (`terraform/aws/demo-instances` module)

```sh
# Create all resources with Terraform
cd terraform/aws
terraform plan
terraform apply
```

#### Optional: Override default schedule of instances

Our policies are defined with `opt-out=false` mode (the default). This means that Cloud custodian applies policies only on tagged resources.

In this demo we use the tag `custodian_scheduled` tag.

- If an instance is NOT tagged with `custodian_scheduled`, it is ignored by Custodian.
- If an instance is tagged with `custodian_scheduled` and empty tag value or `custodian_scheduled=on`, we apply the default schedule, as defined in the policy.

You can use the value of `custodian_scheduled` tag to force a custom schedule.(See <https://cloudcustodian.io/docs/quickstart/offhours.html#tag-based-configuration>).

Example of tag content:

```sh
# using the default schedule of the policy ((empty) or on)
on
#
# up mon-fri from 10am-6pm; cet
off=(M-F,18);on=(M-F,10);tz=cet
# up mon-fri from 6am-9pm; up sun from 10am-6pm; pacific time
off=[(M-F,21),(U,18)];on=[(M-F,6),(U,10)];tz=pt
```

Tag syntax reference: <https://cloudcustodian.io/docs/usecases/offhours.html#tag-based-configuration>

### Execute policies

#### Manual execution (from local computer)

```sh
# activate cloud custodian venv
source custodian/bin/activate

cd policies/aws
# validate policy
custodian validate manual-ec2-off-hours.yml
# Dry run the policies (no actions executed) to see which resources would match each policy.
custodian run --dryrun --region eu-west-1 --output-dir out manual-ec2-off-hours.yml

# Run policies (with explicit region)
custodian run --region eu-west-1 --output-dir out manual-ec2-off-hours.yml
# Run policies (all regions)
custodian run --region all --output-dir out manual-ec2-off-hours.yml

```

#### Automate execution (deploy policies as lambda)

```sh
# Deploy scheduled (automated) policy using a specific aws cli profile (default in this case)
# This will create and schedule a lambda for each policy, using the role described in the policy file
custodian run --profile default --output-dir out auto-ec2-off-hours.yml
```

## Caveats

### The demo code use specific tags

In this demo, policies are applied _only_ on resources tagged with `custodian_managed` or `ec2-off-hours.yml`tags.
A total of four EC2 instances are created in the sample terraform script.
Remember to delete _all instances_ after testing to avoid extra costs.

### Testing automatic off hours / on hours policy

When a policy uses an `on_hour` or `off_hour` filter, the filter match is true _only_ when actual _integer_ part of the hour of the machine that execute custodian matches the parameter of the policy.

For example, a policy that set off_hour at 18 will **not** trigger action if run at 19:xx or after (but will trigger the action if run between 18:00 and 18:59).

The policy is **evaluated hourly**. This allows a developer to restart manually a machine that would have been automatically shutdown and leave it as this until the next day.
See <https://cloudcustodian.io/docs/quickstart/offhours.html#resume-during-offhours>

### Preventing shutdown of an instance that was manually restarted

In some cases, we may want to allow a user to restart an instance outside the default schedule.
If you use automatic policies, you will need to prevent the just restarted instance from being shut down again on every automated run.

Custodian documentation is not very clear on this point as one page mention the need to filter explicitly (<https://cloudcustodian.io/docs/aws/examples/ec2offhours.html?highlight=offhours#ec2-offhours-support>) while the other says it should work out of the box (<https://cloudcustodian.io/docs/aws/examples/offhours.html#resume-during-offhours>).

### Resources

See <https://github.com/1Strategy/cloud-custodian-demo> for sample custodian policies and related lambda permissions.

AWS provides an alternative _AWS Solution_ named [AWS instance scheduler](https://aws.amazon.com/solutions/instance-scheduler/). However Cloud custodian is more flexible to support other kind of policies and can be used with multiple cloud vendors.
