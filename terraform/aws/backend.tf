terraform {
  backend "s3" {
    bucket         = "c7n-demo-yzplzi7v3obthwv-state-bucket"
    dynamodb_table = "c7n-demo-yzplzi7v3obthwv-state-lock"
    region         = "eu-west-1"
    role_arn       = "arn:aws:iam::814098365754:role/c7n-demo-yzplzi7v3obthwv-tf-assume-role"

    encrypt = true

    # Update key to have a tfstate file for your specific environment
    key = "production/terraform.tfstate"
  }
}
