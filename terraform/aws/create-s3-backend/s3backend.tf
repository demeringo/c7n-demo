/*
Initial setup

Create an S3 backend and DynamoDB table for Terraform.

This backend will save and share Terraform state and allow locking mechanism with multiple users.

*/
provider "aws" {
  region = "eu-west-1"
}
module "s3backend" {
  source    = "scottwinkler/s3backend/aws"
  namespace = "c7n-demo"
}
output "s3backend_config" {
  value = module.s3backend.config
}
