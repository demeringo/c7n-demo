provider "aws" {
  region = "eu-west-1"
}

module "custodian-infra" {
  source = "./custodian-infra"
}


module "demo-instances" {
  source = "./demo-instances"
}
