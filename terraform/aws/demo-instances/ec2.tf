
resource "aws_instance" "affected-by-custodian" {
  ami           = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"

  tags = {
    about_tag = "Managed by custodian, uses default schedule of the policy",
    /* The value of custodian_scheduled tag could be empty.
    It would still have the same effect as we are working in opt-out=false mode.*/
    custodian_scheduled = "on"
  }
}

resource "aws_instance" "not-custodian" {
  ami           = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"

  tags = {
    about_tag = "NOT managed by custodian because custodian_scheduled is not present"
  }
}

resource "aws_instance" "lambda-managed-with-default-schedule" {
  ami           = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"

  tags = {
    about_tag           = "Managed by custodian, uses default schedule of the policy",
    custodian_scheduled = ""
  }
}

resource "aws_instance" "lambda-managed-with-custom-schedule" {
  ami           = data.aws_ami.amazon-linux-2.id
  instance_type = "t2.micro"

  tags = {
    about_tag           = "Managed by custodian, uses custom schedule provided by value of custodian_scheduled tag",
    custodian_scheduled = "off=(M-F,18);on=(M-F,10);tz=cet"
  }
}


# Get latest Amazon Linux 2 AMI
data "aws_ami" "amazon-linux-2" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm*"]
  }
}
